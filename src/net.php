<?php

if (!function_exists('lambda')) {
    function lambda($endpoint, $params = [], $autoRetry = TRUE, $method = 'get') {
        $proxy = getenv('LAMBDA_URL') ?: die('LAMBDA_URL proxy is not set');

        for ($i = 0; $i < 3; $i++) {
            $response = curl(sprintf('%s/%s.php', rtrim($proxy, '/'), $endpoint), $method, $params);

            if (($data = json_decode($response, TRUE)) || (!$autoRetry)) {
                return $data;
            } else {
                echo "error: ", $response, "\n";
                //log_error($response);
            }
        }

        return [];
    }
}

if (!function_exists('bing')) {
    /**
     * Do a bing search
     *
     * $results = [['title'=>'','link'=>'','description'=>'','pubdate'=>'']];
     */
    function bing($keyword) {
        return lambda('bing-search', ['q' => $keyword]);
    }
}


if (!function_exists('bing_images')) {
    /**
     * Do a bing image search
     *
     * Additional options:
     * image_size = 'all|small|medium|large|extra_large'
     * color = 'all|color_only|black_and_white'
     * type = 'all|photograph|clipart|line_drawing|animated_gif|transparent'
     * license = 'all|all_creative_commons|public_domain'
     *
     * $results = [['mediaurl'=>'','link'=>'','title'=>'','size'=>'273 x 160 gif 9kB']];
     */
    function bing_images($keyword, $image_size = 'all', $type = '', $license = 'all', $color = 'all') {
        return lambda('bing-image-search', [
            'q'       => $keyword,
            'options' => json_encode(['image_size' => $image_size, 'color' => $color, 'type' => $type, 'license' => $license]),
        ]);
    }
}

if (!function_exists('bing_suggest')) {
    /**
     * Get bing suggestions
     */
    function bing_suggest($keyword) {
        return lambda('bing-suggest', ['q' => $keyword]);
    }
}

if (!function_exists('bing_videos')) {
    /**
     * Do a bing video search
     *
     * $results = [["title"=>'', "duration"=>'', "views"=>'', "link"=>'', "videoid"=>'', "thumbnail"=>'', "thumbnail_mq"=>'', "thumbnail_hq"=>'']];
     */
    function bing_videos($keyword) {
        return lambda('bing-video-search', ['q' => $keyword]);
    }
}

if (!function_exists('cache_online')) {
    /**
     * Stores a variable in online cache
     *
     * $result = cache_online('testing', function() { return 'fallback'; }, 86400);
     */
    function cache_online($key, $value = NULL, $expires = 86400 * 30) {
        if (isset($value)) {
            return lambda('cache', ['key' => $key, 'value' => $value instanceof \Closure ? $value() : $value, 'expiry' => $expires], FALSE, 'post');
        } else {
            return lambda('cache', ['key' => $key, 'expiry' => $expires], FALSE);
        }
    }
}

if (!function_exists('dump_online')) {
    /**
     * Returns a URL that can be used to dump any results online
     *
     * $url = dump_online('my_paypal_ipn'); //returns a URL that can be assigned to Paypal's IPN (to view results, just append _mode=view to dump URL)
     */
    function dump_online($key, $mode = 'save', $passthru = NULL) {
        $url = sprintf('%s/dump.php?_key=%s', getenv('LAMBDA_URL'), $key);

        if ($mode === 'save') {
            return !empty($passthru) ? "$url&_passthru=$passthru" : $url;
        } else {
            return "$url&_mode=view";
        }
    }
}


if (!function_exists('quick_host')) {
    /**
     * Quickly hosts a file on AWS Lambda (using DynamoDB backend)
     *
     * $url = quick_host('this is a text', 'text/plain');
     */
    function quick_host($body, $mime = 'text/plain') {
        $key = md5($body);
        lambda('pastebin', ['key' => $key, 'value' => $body, 'mime' => $mime], FALSE, 'post');

        return sprintf('%s/pastebin.php?key=%s', getenv('LAMBDA_URL'), $key);
    }
}

if (!function_exists('definition')) {
    /**
     * Returns the definition of any term
     */
    function definition($term, $max_sentences = 2, $lang = 'en-US') {
        return lambda('define', ['q' => $term, 'sentences' => $max_sentences, 'lang' => $lang]);
    }
}

if (!function_exists('google')) {
    /**
     * Do a google search
     *
     * $types = curl('https://raw.githubusercontent.com/serp-spider/search-engine-google/master/src/NaturalResultType.php');
     */
    function google($keyword, $lang = 'en-US', $num = 10, $types = ['classical', 'classical_large']) {
        return lambda('google-search', ['q' => $keyword, 'lang' => $lang, 'num' => $num, 'types' => json_encode($types)]);
    }
}

if (!function_exists('google_images')) {
    /**
     * Do a google image search
     *
     * $images = google_images('test filetype:svg');
     */
    function google_images($keyword) {
        return lambda('google-image-search', ['q' => $keyword]);
    }
}

if (!function_exists('google_suggest')) {
    /**
     * Get google suggestions
     */
    function google_suggest($keyword) {
        return lambda('google-suggest', ['q' => $keyword]);
    }
}

if (!function_exists('pexel')) {
    /**
     * Do pexel image search
     *
     * $images = pexel('business'[, true = urls list, false = with details like id, width, height, url, photographer, src[original|large|medium]);
     */
    function pexel($keyword, $image_urls_only = FALSE, $num = 40) {
        return lambda('pexel-image-search', ['q' => $keyword, 'options' => json_encode(['per_page' => $num]), 'simple' => $image_urls_only ? 'true' : '']);
    }
}

if (!function_exists('tts')) {
    /**
     * Generate text to speech using Google TTS or Amazon Polly
     *
     * $url = tts('google', 'yes sir', 'en-US-Wavenet-D', 'en-US' (required by Google only));
     */
    function tts($provider = 'polly', $text = 'hi there!', $voice = 'Raveena', $lang = 'en-US') {
        $response = lambda("$provider-tts", ['text' => $text, 'voice' => $voice, 'lang' => $lang]);
        return $response['data'] ?? FALSE;
    }
}

if (!function_exists('tts_voices')) {
    /**
     * List available tts voices in Google TTS or Amazon Polly
     */
    function tts_voices($provider = 'polly', $lang = 'english', $wavenet = TRUE) {
        return lambda("$provider-tts", ['voices' => 1, 'filter' => $lang, 'all' => $wavenet ? '' : '1']);
    }
}

if (!function_exists('wikipedia')) {
    /**
     * Do a wikipedia search
     */
    function wikipedia($term, $sentences = 3) {
        $response = lambda('wikipedia-search', ['q' => $term, 'sentences' => $sentences]);
        return remove_parens($response['text'] ?? '');
    }
}

if (!function_exists('youtube')) {
    /**
     * Search video on youtube
     *
     * $results = youtube('term', false, length = [any, long, medium, short], order = [date, rating, relevance, title, videoCount, viewCount]);
     */
    function youtube($term, $details = FALSE, $length = 'any', $num = 20, $type = 'video', $order = 'relevance') {
        return lambda('youtube-search', ['q' => $term, 'detailed' => $details ? 1 : '', 'length' => $length, 'order' => $order, 'params' => json_encode(['type' => $type, 'num' => $num])]);
    }
}

if (!function_exists('youtube_dl')) {
    /**
     * Get the downloadable URL of any Youtube video
     */
    function youtube_dl($youtubeId) {
        $response = lambda('youtube-dl', ['id' => $youtubeId]);
        return $response['url'] ?? FALSE;
    }
}

if (!function_exists('youtube_video_details')) {
    /**
     * Returns detailed metadata of Youtube video
     *
     * $return = ['title', 'description', 'keywords'(array), 'category', 'thumbnail', 'length' (secs), 'subs'(true|false), 'res'(hd|sd), 'embed'(true|false)', player(embed code), 'stats'=>['likes','viewCount',...]];
     */
    function youtube_video_details($youtubeId) {
        return lambda('youtube-video-detail', ['id' => $youtubeId]);
    }
}

if (!function_exists('google_auth')) {
    /**
     * Authenticates an email or access_token (emails are permanently associated with token for future).
     *
     * $result = google_auth('email@example.com', [scopes.., $redirect); $redirect will be set if not previously authenticated (otherwise Google_Client is returned).
     */
    function google_auth($email_or_token_or_client, $scopes, &$redirect = NULL) {
        if (!class_exists('Google_Client')) {
            die('Please install google-api first: composer require google/apiclient');
        }

        try {
            $OAUTH2_CLIENT_ID = getenv('GOOGLE_CLIENT_ID') ?: 'GOOGLE_CLIENT_ID is not defined';
            $OAUTH2_CLIENT_SECRET = getenv('GOOGLE_CLIENT_SECRET') ?: 'GOOGLE_CLIENT_SECRET is not defined';

            if ($email_or_token_or_client instanceof Google_Client) {
                $client = $email_or_token_or_client;
                $accessToken = $client->getAccessToken();
            } else {
                $client = new Google_Client();
                $client->setClientId($OAUTH2_CLIENT_ID);
                $client->setClientSecret($OAUTH2_CLIENT_SECRET);
                $client->setScopes($scopes);
                $client->setAccessType('offline');
                $client->setRedirectUri(self_uri());

                if (is_scalar($email_or_token_or_client)) {
                    $cacheKey = 'gauth-' . md5($email_or_token_or_client . '-' . $client->prepareScopes());
                    $redirect = $client->createAuthUrl();
                    $saveToken = function ($accessToken) use ($cacheKey) {
                        cache_online($cacheKey, encrypt(json_encode($accessToken)));
                    };

                    if (isset($_GET['code'])) {
                        if ($accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code'])) {
                            $saveToken($accessToken);
                        }
                    } elseif ($data = cache_online($cacheKey)) {
                        $accessToken = json_decode(decrypt($data), TRUE);
                    }
                } elseif (is_array($email_or_token_or_client)) {
                    $accessToken = $email_or_token_or_client;
                }
            }

            if (!empty($accessToken)) {
                $client->setAccessToken($accessToken);

                if ($client->isAccessTokenExpired() && !empty($accessToken['refresh_token'])) {
                    $client->refreshToken($accessToken['refresh_token']);
                    if (($accessToken = $client->getAccessToken()) && !empty($saveToken)) {
                        $saveToken($accessToken);
                    }
                }

                if ($client->getAccessToken()) {
                    $redirect = NULL;
                    return $client;
                }
            }
        } catch (\Throwable $e) {
            debug('Google auth error: ' . $e->getMessage());
        }

        return FALSE;
    }
}

if (!function_exists('google_user_info')) {
    /**
     * Returns the name and email of an authenticated Google_Client user
     *
     * $result = google_user_info($client); //['name' => '..', 'first' => '..', 'email' => ''];
     */
    function google_user_info($email_or_token_client, &$redirect = NULL) {
        if ($client = google_auth($email_or_token_client, ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me'], $redirect)) {
            $plus = new Google_Service_Plus($client);
            $person = $plus->people->get('me');

            if ($name = $person->getName()) {
                $result['first'] = $name->getGivenName();
                $result['last'] = $name->getFamilyName();
                $result['name'] = trim($name->getGivenName() . " " . $name->getFamilyName());
            }

            if ($emails = $person->getEmails()) {
                $result['email'] = $emails[0]->getValue();
            }

            return $result ?? [];
        }
    }
}

if (!function_exists('youtube_categories')) {
    /**
     * Returns the list of youtube categories and their associated ids (if filter is given only returns matching category id).
     *
     * $cat_id = youtube_categories('howto'); //26
     */
    function youtube_categories($filter = NULL) {
        $categories = ['Film & Animation' => '1', 'Autos & Vehicles' => '2', 'Music' => '10', 'Pets & Animals' => '15', 'Sports' => '17', 'Short Movies' => '18', 'Travel & Events' => '19', 'Gaming' => '20', 'Videoblogging' => '21', 'People & Blogs' => '22', 'Comedy' => '34', 'Entertainment' => '24', 'News & Politics' => '25', 'Howto & Style' => '26', 'Education' => '27', 'Science & Technology' => '28', 'Nonprofits & Activism' => '29', 'Movies' => '30', 'Anime/Animation' => '31', 'Action/Adventure' => '32', 'Classics' => '33', 'Documentary' => '35', 'Drama' => '36', 'Family' => '37', 'Foreign' => '38', 'Horror' => '39', 'Sci-Fi/Fantasy' => '40', 'Thriller' => '41', 'Shorts' => '42', 'Shows' => '43', 'Trailers' => '44',];

        if (!empty($filter)) {
            foreach ($categories as $key => $value) {
                if (preg_match("/$filter/i", $key)) {
                    return $value;
                }
            }

            return FALSE;
        }

        return $categories;
    }
}

if (!function_exists('youtube_upload')) {
    /**
     * Uploads a video to YouTube (optionally saves token if email is given)
     * $url = youtube_upload('c:/test.mp4', 'email@example.com', ['title' => 'test'], $redirect); //redirect url is set if email isn't authenticated
     */
    function youtube_upload(string $videoPath, $email_or_token_or_client, array $attrs = [], &$redirect = NULL) {
        if (realpath($videoPath)) {
            $scopes = ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.upload'];

            if ($client = google_auth($email_or_token_or_client, $scopes, $redirect)) {
                try {
                    $youtube = new Google_Service_YouTube($client);
                    $category = function ($name) {
                        foreach (youtube_categories() as $key => $value) {
                            if (preg_match("/$name/i", $key)) {
                                return $value;
                            }
                        }

                        return FALSE;
                    };

                    $snippet = new Google_Service_YouTube_VideoSnippet();
                    $snippet->setTitle($attrs['title'] ?? "Test title");
                    $snippet->setDescription($attrs['description'] ?? "Test description");
                    $snippet->setTags($attrs['tags'] ?? ["tag1", "tag2"]);
                    $snippet->setCategoryId(@is_numeric($attrs['category']) ? $attrs['category'] : ($category($attrs['category'] ?? 'Howto') ?: 26));
                    $status = new Google_Service_YouTube_VideoStatus();
                    $status->privacyStatus = $attrs['status'] ?? "public"; //"public", "private" and "unlisted"

                    $video = new Google_Service_YouTube_Video();
                    $video->setSnippet($snippet);
                    $video->setStatus($status);

                    $chunkSizeBytes = 5 * 1024 * 1024;

                    $client->setDefer(TRUE);
                    /** @var \Psr\Http\Message\RequestInterface $insertRequest */
                    $insertRequest = $youtube->videos->insert("status,snippet", $video);
                    $media = new Google_Http_MediaFileUpload($client, $insertRequest, 'video/*', NULL, TRUE, $chunkSizeBytes);
                    $media->setFileSize(filesize($videoPath));
                    $status = FALSE;

                    $handle = fopen($videoPath, "rb");
                    while (!$status && !feof($handle)) {
                        $chunk = fread($handle, $chunkSizeBytes);
                        $status = $media->nextChunk($chunk);
                    }

                    fclose($handle);

                    $client->setDefer(FALSE);

                    return !empty($status['id']) ? sprintf("http://www.youtube.com/watch?v=%s", $status['id']) : FALSE;
                } catch (\Throwable $e) {
                    debug('Upload error: ' . $e->getMessage());
                }
            }
        } else {
            debug('File not found: ' . $videoPath);
        }

        return FALSE;
    }
}

if (!function_exists('ip_info')) {
    /**
     * Returns ip information by ip
     */
    function ip_info($ip = NULL, $basic = TRUE) {
        return ['country' => 'India']; //TODO remove
        $response = lambda("ip-info", ['ip' => $ip ?? ip_address()]);

        if (!empty($response['country']) || !empty($response['country_name'])) {
            foreach ($response as $key => $value) {
                if ($key === 'org') $key = 'isp';
                if ($key === 'country') $key = 'country_name';
                if ($key === 'query') continue;
                if (!empty($basic) && !preg_match('/(country|city|state|isp)/', $key)) continue;

                $results[strtr(kebab($key), ['-' => '_'])] = $value;
            }

            return $results;
        }

        return [];
    }
}

if (!function_exists('phone_call')) {
    /**
     * Calls a number and plays a message (requires Twilio API).
     * Input url: The url to call with digits (phone call will end requiring user input)
     * Record url: The url to call with voice recording (phone call will end requiring user to speak)
     *
     * phone_call('+phone', 'Hello! Please record after beep', NULL, NULL, 'https://buzzvid.localtunnel.me/dump');
     */
    function phone_call($to, $msg = 'Hi there!', $from = NULL, $input_url = NULL, $record_url = NULL) {
        $sid = getenv('TWILIO_KEY') ?: die('TWILIO_KEY (Sid) is not present');
        $token = getenv('TWILIO_SECRET') ?: die('TWILIO_SECRET (Auth token) is not present');
        $from = $from ?? (getenv('TWILIO_FROM') ?: die('TWILIO_FROM (Auth token) is not present'));

        if (strpos($to, '+') !== 0 || strpos($from, '+') !== 0) {
            die('country code is required in from and to');
        }

        $body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response>";
        $body .= "<Say>$msg</Say>";
        $make = function ($url) {
            return htmlentities(sprintf('https://www.queries.review/dump.php?_key=%s&_response=%s&_passthru=%s', md5($url), urlencode('<Response><Say>Thank you</Say></Response>'), urlencode($url)));
        };

        if (!empty($input_url)) {
            $body .= "<Gather method=\"GET\" input=\"dtmf\" timeout=\"5\" action=\"" . $make($input_url) . "\"><Say>Press the # key to finish.</Say></Gather>";
        }

        if (!empty($record_url)) {
            $body .= "<Record method=\"GET\" timeout=\"10\" action=\"" . $make($record_url) . "\"><Say>Press the # key to finish recording.</Say></Record>";;
        }

        $body .= "</Response>";
        $url = quick_host($body, 'text/xml');

        curl("https://api.twilio.com/2010-04-01/Accounts/$sid/Calls.json", 'post',
            ['To' => $to, 'From' => $from, 'Url' => $url],
            ['Authorization: Basic ' . base64_encode("$sid:$token")],
            $status
        );

        return $status === 201;
    }
}

if (!function_exists('ping_owner')) {
    /**
     * Sends site owner an email / sms in case of emergencies
     *
     * $example = ping_owner('site is not pinging!', "what's up?", 0.5); //notify every 1/2 hour
     */
    function ping_owner($subject, $html = '', $min_gap_hours = 6, $phone = TRUE) {
        $key = 'error-' . md5($subject);
        $lastNotify = cache_online($key);

        if (!$lastNotify || ((time() - $lastNotify) > ($min_gap_hours * 60 * 60))) {
            cache_online($key, time());

            try {
                throw new Exception('Ping');
            } catch (\Exception $e) {
                $error = $e->getTraceAsString();
            }

            $host = $_SERVER['HTTP_HOST'] ?? 'localhost';
            $to = $to ?? (getenv('OWNER_EMAIL') ?: sprintf('support@%s', $host));
            $from = $from ?? (getenv('OWNER_EMAIL') ?: sprintf('support@%s', $host));
            $info = "website: $host\n\nTrace:\n$error";

            try {
                email($to, $from, "$subject ($host)", ($html ?: $subject) . "\n\nMore info:\n$info", FALSE);
            } catch (\Exception $e) {
                debug("Error sending email: " . $e->getMessage());
            }

            if (!empty($phone) && !empty(getenv('OWNER_PHONE')) && !empty(getenv('TWILIO_KEY')) && !empty(getenv('TWILIO_SECRET')) && !empty(getenv('TWILIO_FROM'))) {
                phone_call(getenv('OWNER_PHONE'), "$subject - $host");
            }
        }
    }
}


//require_once '../../all/src/everything.php'; //don't remove - only comment (helps in testing)