## Installation

Just run `composer install [package_name]`

After that all these functions should be available in every PHP file (via composer's autoloader)

- [bing](#bing)

- [bing_images](#bing_images)

- [bing_suggest](#bing_suggest)

- [bing_videos](#bing_videos)

- [cache_online](#cache_online)

- [dump_online](#dump_online)

- [quick_host](#quick_host)

- [definition](#definition)

- [google](#google)

- [google_images](#google_images)

- [google_suggest](#google_suggest)

- [pexel](#pexel)

- [tts](#tts)

- [tts_voices](#tts_voices)

- [wikipedia](#wikipedia)

- [youtube](#youtube)

- [youtube_dl](#youtube_dl)

- [youtube_video_details](#youtube_video_details)

- [google_auth](#google_auth)

- [google_user_info](#google_user_info)

- [youtube_categories](#youtube_categories)

- [youtube_upload](#youtube_upload)

- [ip_info](#ip_info)

- [phone_call](#phone_call)

- [ping_owner](#ping_owner)

### bing

Do a bing search


```php
function bing($keyword)
```


Example:

```php
$results = [['title'=>'','link'=>'','description'=>'','pubdate'=>'']];
```

### bing_images

Do a bing image search

 Additional options:
 image_size = 'all|small|medium|large|extra_large'
 color = 'all|color_only|black_and_white'
 type = 'all|photograph|clipart|line_drawing|animated_gif|transparent'
 license = 'all|all_creative_commons|public_domain'


```php
function bing_images($keyword, $image_size = 'all', $type = '', $license = 'all', $color = 'all')
```


Example:

```php
$results = [['mediaurl'=>'','link'=>'','title'=>'','size'=>'273 x 160 gif 9kB']];
```

### bing_suggest

Get bing suggestions

```php
function bing_suggest($keyword)
```


### bing_videos

Do a bing video search


```php
function bing_videos($keyword)
```


Example:

```php
$results = [["title"=>'', "duration"=>'', "views"=>'', "link"=>'', "videoid"=>'', "thumbnail"=>'', "thumbnail_mq"=>'', "thumbnail_hq"=>'']];
```

### cache_online

Stores a variable in online cache


```php
function cache_online($key, $value = NULL, $expires = 86400 * 30)
```


Example:

```php
$result = cache_online('testing', function() { return 'fallback'; }, 86400);
```

### dump_online

Returns a URL that can be used to dump any results online


```php
function dump_online($key, $mode = 'save', $passthru = NULL)
```


Example:

```php
$url = dump_online('my_paypal_ipn'); //returns a URL that can be assigned to Paypal's IPN (to view results, just append _mode=view to dump URL)
```

### quick_host

Quickly hosts a file on AWS Lambda (using DynamoDB backend)


```php
function quick_host($body, $mime = 'text/plain')
```


Example:

```php
$url = quick_host('this is a text', 'text/plain');
```

### definition

Returns the definition of any term

```php
function definition($term, $max_sentences = 2, $lang = 'en-US')
```


### google

Do a google search


```php
function google($keyword, $lang = 'en-US', $num = 10, $types = ['classical', 'classical_large'])
```


Example:

```php
$types = curl('https://raw.githubusercontent.com/serp-spider/search-engine-google/master/src/NaturalResultType.php');
```

### google_images

Do a google image search


```php
function google_images($keyword)
```


Example:

```php
$images = google_images('test filetype:svg');
```

### google_suggest

Get google suggestions

```php
function google_suggest($keyword)
```


### pexel

Do pexel image search


```php
function pexel($keyword, $image_urls_only = FALSE, $num = 40)
```


Example:

```php
$images = pexel('business'[, true = urls list, false = with details like id, width, height, url, photographer, src[original|large|medium]);
```

### tts

Generate text to speech using Google TTS or Amazon Polly


```php
function tts($provider = 'polly', $text = 'hi there!', $voice = 'Raveena', $lang = 'en-US')
```


Example:

```php
$url = tts('google', 'yes sir', 'en-US-Wavenet-D', 'en-US' (required by Google only));
```

### tts_voices

List available tts voices in Google TTS or Amazon Polly

```php
function tts_voices($provider = 'polly', $lang = 'english', $wavenet = TRUE)
```


### wikipedia

Do a wikipedia search

```php
function wikipedia($term, $sentences = 3)
```


### youtube

Search video on youtube


```php
function youtube($term, $details = FALSE, $length = 'any', $num = 20, $type = 'video', $order = 'relevance')
```


Example:

```php
$results = youtube('term', false, length = [any, long, medium, short], order = [date, rating, relevance, title, videoCount, viewCount]);
```

### youtube_dl

Get the downloadable URL of any Youtube video

```php
function youtube_dl($youtubeId)
```


### youtube_video_details

Returns detailed metadata of Youtube video


```php
function youtube_video_details($youtubeId)
```


Example:

```php
$return = ['title', 'description', 'keywords'(array), 'category', 'thumbnail', 'length' (secs), 'subs'(true|false), 'res'(hd|sd), 'embed'(true|false)', player(embed code), 'stats'=>['likes','viewCount',...]];
```

### google_auth

Authenticates an email or access_token (emails are permanently associated with token for future).


```php
function google_auth($email_or_token_or_client, $scopes, &$redirect = NULL)
```


Example:

```php
$result = google_auth('email@example.com', [scopes.., $redirect); $redirect will be set if not previously authenticated (otherwise Google_Client is returned).
```

### google_user_info

Returns the name and email of an authenticated Google_Client user


```php
function google_user_info($email_or_token_client, &$redirect = NULL)
```


Example:

```php
$result = google_user_info($client); //['name' => '..', 'first' => '..', 'email' => ''];
```

### youtube_categories

Returns the list of youtube categories and their associated ids (if filter is given only returns matching category id).


```php
function youtube_categories($filter = NULL)
```


Example:

```php
$cat_id = youtube_categories('howto'); //26
```

### youtube_upload

Uploads a video to YouTube (optionally saves token if email is given)


```php
function youtube_upload(string $videoPath, $email_or_token_or_client, array $attrs = [], &$redirect = NULL)
```


Example:

```php
$url = youtube_upload('c:/test.mp4', 'email@example.com', ['title' => 'test'], $redirect); //redirect url is set if email isn't authenticated
```

### ip_info

@var \Psr\Http\Message\RequestInterface $insertRequest *

```php
function ip_info($ip = NULL, $basic = TRUE)
```


### phone_call

Calls a number and plays a message (requires Twilio API).
 Input url: The url to call with digits (phone call will end requiring user input)
 Record url: The url to call with voice recording (phone call will end requiring user to speak)

 phone_call('+phone', 'Hello! Please record after beep', NULL, NULL, 'https://buzzvid.localtunnel.me/dump');

```php
function phone_call($to, $msg = 'Hi there!', $from = NULL, $input_url = NULL, $record_url = NULL)
```


### ping_owner

Sends site owner an email / sms in case of emergencies


```php
function ping_owner($subject, $html = '', $min_gap_hours = 6, $phone = TRUE)
```


Example:

```php
$example = ping_owner('site is not pinging!', "what's up?", 0.5); //notify every 1/2 hour
```

